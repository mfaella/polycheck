#include "main.h"
#include "polyread.h"

// Before executing:
// export LD_LIBRARY_PATH=/home/mfaella/Projects/PPL/installed/lib
// Example usage:
// crop -7 7 -2 7 -f Start-4D.poly x1 y1 x2=6 y2=4

int VERBOSITY = 0;

void dump_map(Varmap& map) {
  for ( Varmap::const_iterator it = map.begin(); it != map.end(); it++) {
    cerr << "Key " << it->first;
    cerr << " has value " << it->second << endl;
  }
}


int main(int argc, char* argv[])
{
  Variables_Set varset;
  Varmap vmap;  
  std::string path[2];
  int nfile = 0;

  try {
    if (argc < 3) throw string("Must provide two filenames.");

    int first_non_option_arg = 1;
    if (!strcmp(argv[1], "-v")) {
      VERBOSITY = 1;
      first_non_option_arg = 2;
    }
    
    int i = first_non_option_arg, j = 0;
    for (; i < argc-2; ++i) {     
      string var_name = argv[i];
      vmap[var_name] = j;
      j++;      
    }
    while (i < argc) {     
      std::string arg = argv[i];
      path[nfile++] = argv[i++];
    }

    std::fstream file;

    PPS region[2];
    for (int i = 0; i<nfile ; i++) {
      cout << "Reading file " << path[i] << endl;
      file.open(path[i].c_str(), ios_base::in);
      if (!file)
	throw string("Error opening file <"+ path[i] + ">.");
      region[i] = read_powerset(file, vmap);
      file.close();
      region[i].pairwise_reduce();
    }

    if (VERBOSITY) {
      cout << "first = " << region[0] << endl;
      cout << "second = " << region[1] << endl;
    }

    if (!region[0].geometrically_covers(region[1])) {
      cout << "Second argument larger" << endl;
      PPS difference = region[1];
      difference.difference_assign(region[0]);
      cout << "Difference = " << difference << endl;
      return 1;
    } else if (!region[1].geometrically_covers(region[0])) {
      cout << "First argument larger" << endl;
      PPS difference = region[0];
      difference.difference_assign(region[1]);
      cout << "Difference = " << difference << endl;
      return 1;
    } else {
      cout << "Equal" << endl;
      return 0;
    }
    
  } catch (string s) {
    cerr << endl << s << endl << endl;
    cerr << "Usage: " << endl;
    cerr << "       " << argv[0] << " [-v] <variable list> <file1> <file2>" << endl << endl;
    cerr << "Example: " << endl;
    cerr << "       " << argv[0] << " x y z first.poly second.poly" << endl << endl;
    cerr << "Input format: " << endl;
    cerr << "       " << "( {x>=1 & y==2} {x+2*y<=3 & x-y >= z} )" << endl << endl;
    return 1;
  }    

  return 0;
}
  

