/* Reads a Powerset (set of polyhedra) from [file], having variables [vars].
*/
PPS read_powerset(std::fstream& file, Varmap vars);

